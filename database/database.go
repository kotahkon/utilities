package database

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	connection *gorm.DB

	tables = map[string]interface{}{
		"target":       TargetPtr,
		"user":         UserPtr,
		"visit_record": VisitRecordPtr,
	}
)

func Get() *gorm.DB {
	return connection
}

func Open(postgresAddress string) (err error) {
	connection, err = gorm.Open("postgres", postgresAddress)
	if err != nil {
		return err
	}
	if os.Getenv("DATABASE_LOG") == "true" {
		connection.LogMode(true)
	}

	for _, table := range tables {
		if !connection.HasTable(table) {
			err = connection.CreateTable(table).Error
			if err != nil {
				return err
			}
		}
		err = connection.AutoMigrate(table).Error
		if err != nil {
			return err
		}
	}
	return nil
}

func Model(a interface{}) *gorm.DB {
	return Get().Model(a)
}
