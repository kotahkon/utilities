package database

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/segmentio/ksuid"
)

type User struct {
	ID        uint       `gorm:"primary_key" json:"id"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"-" sql:"index"`
	UID       string     `json:"uid" gorm:"unique" cache:"uid"`
	Email     string     `json:"email" gorm:"unique"`
	Username  string     `json:"username" gorm:"unique"`
	Password  string     `json:"-"`
}

var UserPtr = new(User)

func (s *User) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("UID", ksuid.New().String())
	return nil
}

func (s *User) Exists() bool {
	if s == nil {
		return false
	}
	return s.ID != 0 && s.UID != ""
}
