package database

import (
	"crypto/sha256"
	"encoding/hex"
	"math/rand"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/kotahkon/utilities/browser"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func sha256hash(text string) string {
	hasher := sha256.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

type VisitRecord struct {
	ID        uint      `gorm:"primary_key" json:"id"`
	CreatedAt time.Time `json:"created_at"`
	TargetID  uint      `json:"-"`
	Target    *Target   `json:"target,omitempty"`
	IPAddress string    `json:"ip_address"`
	Checksum  string    `json:"checksum"`
	browser.Browser
}

var VisitRecordPtr = new(VisitRecord)

func (s *VisitRecord) Exists() bool {
	if s == nil {
		return false
	}
	return s.ID != 0 && s.TargetID != 0
}

func (s *VisitRecord) BeforeCreate(scope *gorm.Scope) error {
	scope.SetColumn("Checksum", sha256hash(s.IPAddress+s.BrowserName+s.Platform))
	return nil
}
