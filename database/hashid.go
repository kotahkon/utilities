package database

import (
	"log"
	"os"

	"github.com/speps/go-hashids"
)

var hashIDGenerator *hashids.HashID

func init() {
	var err error

	hd := hashids.NewData()
	hd.Salt = os.Getenv("SECRET_KEY")
	hd.Alphabet = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNxXyYzZ1234567890"
	hd.MinLength = 3
	hashIDGenerator, err = hashids.NewWithData(hd)
	if err != nil {
		log.Fatal(err)
	}
}
