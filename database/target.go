package database

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Target struct {
	ID        uint       `gorm:"primary_key" json:"id"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"-" sql:"index"`
	TargetURL string     `json:"target_url"`
	ShortID   string     `json:"short_id" gorm:"unique" cache:"short_id"`
	CreatorID uint       `json:"-"`
	Creator   *User      `json:"creator"`
	Visits    uint       `json:"visits"`
}

var TargetPtr = new(Target)

func (t *Target) AfterCreate(tx *gorm.DB) error {
	if t.ShortID == "" {
		shortID, err := hashIDGenerator.Encode([]int{int(t.ID)})
		if err != nil {
			return err
		}
		tx.Model(t).Update("short_id", shortID)
	}
	return nil
}
