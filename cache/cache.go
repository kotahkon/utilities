package cache

import (
	"errors"
	"time"

	rcache "github.com/go-redis/cache"
	"github.com/go-redis/redis"
	"github.com/vmihailenco/msgpack"
)

type Cache struct {
	codec             *rcache.Codec
	redis             *redis.Client
	DefaultExpiration time.Duration
}

var c *Cache

func New(redisAddr string) {
	redisInstance := redis.NewClient(&redis.Options{
		Addr:     redisAddr,
		Password: "",
		DB:       0,
	})
	codec := &rcache.Codec{
		Redis: redisInstance,
		Marshal: func(v interface{}) ([]byte, error) {
			return msgpack.Marshal(v)
		},
		Unmarshal: func(b []byte, v interface{}) error {
			return msgpack.Unmarshal(b, v)
		},
	}
	c = &Cache{
		codec:             codec,
		redis:             redisInstance,
		DefaultExpiration: 15 * time.Minute,
	}
}

func Redis() *redis.Client {
	return c.redis
}

func Ping() error {
	result, err := c.redis.Ping().Result()
	if err != nil {
		return err
	}
	if result != "PONG" {
		return errors.New("bad PONG result")
	}
	return nil
}

func Del(key string) error {
	if Has(key) {
		return c.codec.Delete(key)
	}
	return nil
}

func Keys(pattern string) []string {
	return c.redis.Keys(pattern).Val()
}

func DelPattern(pattern string) {
	c.redis.Del(Keys(pattern)...)
}

func Has(key string) bool {
	return c.codec.Exists(key)
}

func Set(key string, value interface{}, exp ...time.Duration) error {
	expiration := c.DefaultExpiration
	if len(exp) > 0 {
		expiration = exp[0]
	}
	return c.codec.Set(&rcache.Item{
		Key:        key,
		Object:     value,
		Expiration: expiration,
	})
}

func Get(key string, output interface{}) error {
	return c.codec.Get(key, output)
}
