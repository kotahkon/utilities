package validator

import "regexp"

const (
	linkPattern     = `(?:(?:https?:\/\/)(?:[a-z0-9.\-]+|www|[a-z0-9.\-])[.](?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))*\))+(?:\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))*\)|[^\s!()\[\]{};:\'".,<>?]))`
	shortIDPattern  = `([A-Za-z0-9]){3,}`
	uuidPattern     = `[0-9a-fA-F]{8}-?[a-fA-F0-9]{4}-?[a-fA-F0-9]{4}-?[a-fA-F0-9]{4}-?[a-fA-F0-9]{12}`
	emailPattern    = `(?i)([A-Za-z0-9!#$%&'*+\/=?^_{|.}~-]+@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)`
	usernamePattern = `([A-Za-z0-9]){4,}`
	passwordPattern = `.{6,}`
)

var (
	linkRegex     = regexp.MustCompile(linkPattern)
	shortIDRegex  = regexp.MustCompile(shortIDPattern)
	uuidRegex     = regexp.MustCompile(uuidPattern)
	emailRegex    = regexp.MustCompile(emailPattern)
	usernameRegex = regexp.MustCompile(usernamePattern)
	passwordRegex = regexp.MustCompile(passwordPattern)
)

func Link(input string) bool {
	return linkRegex.MatchString(input)
}

func ShortID(input string) bool {
	return shortIDRegex.MatchString(input)
}

func UUID(input string) bool {
	return uuidRegex.MatchString(input)
}

func Email(input string) bool {
	return emailRegex.MatchString(input)
}

func Username(input string) bool {
	return usernameRegex.MatchString(input)
}

func Password(input string) bool {
	return passwordRegex.MatchString(input)
}
