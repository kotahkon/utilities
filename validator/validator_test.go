package validator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateUUID(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	trueTests := []string{
		"00000000-0000-0000-0000-000000000000",
		"00000000000000000000000000000000",
		"88a310ed-0ac0-4a3d-b3a2-958fa291d061",
		"27143ecab8a440cda6fb6effcf9b3c75",
	}

	falseTests := []string{
		"88z310ed-0ac0-4a3d-b3x2-958fa291d061",
	}

	for _, test := range trueTests {
		assert.True(UUID(test))
	}

	for _, test := range falseTests {
		assert.False(UUID(test))
	}
}

func TestValidateURL(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	tests := []string{
		"http://www.google.com",
		"http://www.google.com/%&#/?q=dog",
	}

	for _, test := range tests {
		assert.True(Link(test))
	}
}

func TestValidateShortID(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	tests := []string{
		"aidenzibaei",
		"HaIw1",
		"jIkaL",
	}

	for _, test := range tests {
		assert.True(ShortID(test))
	}
}
