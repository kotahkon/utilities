package browser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUserAgent(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	type Sample struct {
		UserAgent   string
		BrowserName string
	}
	tests := []Sample{
		{
			UserAgent:   "Mozilla/5.0 (iPhone; CPU iPhone OS 13_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 [FBAN/FBIOS;FBDV/iPhone11,8;FBMD/iPhone;FBSN/iOS;FBSV/13.3.1;FBSS/2;FBID/phone;FBLC/en_US;FBOP/5;FBCR/AT&T]",
			BrowserName: "Safari",
		},
		{
			UserAgent:   "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:24.0) Gecko/20100101 Firefox/24.0",
			BrowserName: "Firefox",
		},
	}

	for _, test := range tests {
		result := Parse(test.UserAgent)
		assert.True(test.BrowserName == result.BrowserName)
	}
}
