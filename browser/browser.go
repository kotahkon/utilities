package browser

import (
	"fmt"

	"github.com/avct/uasurfer"
)

type Browser struct {
	OSName         string `json:"os_name"`
	OSVersion      string `json:"os_version"`
	Platform       string `json:"platform"`
	DeviceType     string `json:"device_type"`
	BrowserName    string `json:"browser_name"`
	BrowserVersion string `json:"browser_version"`
	IsBot          bool   `json:"is_bot"`
}

func versionToString(v uasurfer.Version) string {
	return fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.Patch)
}

func trimString(input, substr string) string {
	return input[len(substr):]
}

func Parse(userAgent string) Browser {
	result := uasurfer.Parse(userAgent)
	isBot := result.IsBot()
	if result.Browser.Name == uasurfer.BrowserUnknown &&
		result.OS.Platform == uasurfer.PlatformUnknown {
		isBot = true
	}
	return Browser{
		OSName:         trimString(result.OS.Name.String(), "OS"),
		OSVersion:      versionToString(result.OS.Version),
		Platform:       trimString(result.OS.Platform.String(), "Platform"),
		DeviceType:     trimString(result.DeviceType.String(), "Device"),
		BrowserName:    trimString(result.Browser.Name.String(), "Browser"),
		BrowserVersion: versionToString(result.Browser.Version),
		IsBot:          isBot,
	}
}
