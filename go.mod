module gitlab.com/kotahkon/utilities

go 1.13

require (
	github.com/avct/uasurfer v0.0.0-20191028135549-26b5daa857f1
	github.com/go-redis/cache v6.4.0+incompatible
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.12
	github.com/nsqio/go-nsq v1.0.8
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/segmentio/ksuid v1.0.2
	github.com/speps/go-hashids v2.0.0+incompatible
	github.com/stretchr/testify v1.5.1
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
)
