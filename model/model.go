package model

import (
	"fmt"
	"reflect"
	"time"

	"gitlab.com/kotahkon/utilities/cache"
)

const cachePrefix = "cache"

func key(modelName string, cacheTag string, value interface{}) string {
	return fmt.Sprintf("%s-%s-%s-%v", cachePrefix, modelName, cacheTag, value)
}

func keys(modelName string, model interface{}) (out []string) {
	out = make([]string, 0)
	item := reflect.Indirect(reflect.ValueOf(model))
	t := item.Type()
	for i := 0; i < t.NumField(); i++ {
		cacheTag, exists := t.Field(i).Tag.Lookup("cache")
		if !exists {
			continue
		}
		value := item.Field(i).Interface()
		out = append(out, key(modelName, cacheTag, value))
	}
	return out
}

func set(modelName string, model interface{}, exp ...time.Duration) {
	for _, key := range keys(modelName, model) {
		cache.Set(key, model, exp...)
	}
}

func get(key string, output interface{}) bool {
	if !cache.Has(key) {
		return false
	}
	return cache.Get(key, output) == nil
}

func del(modelName string, model interface{}) {
	for _, key := range keys(modelName, model) {
		cache.Del(key)
	}
}

func hasSelector(selector string, model interface{}) bool {
	t := reflect.Indirect(reflect.ValueOf(model)).Type()
	for i := 0; i < t.NumField(); i++ {
		cacheTag, exists := t.Field(i).Tag.Lookup("cache")
		if !exists {
			continue
		}
		if cacheTag == selector {
			return true
		}
	}
	return false
}
