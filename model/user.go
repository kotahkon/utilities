package model

import (
	"time"

	"gitlab.com/kotahkon/utilities/database"
)

func GetUser(selector string, value interface{}) *database.User {
	output := &database.User{}
	if !hasSelector(selector, output) {
		return nil
	}
	cacheKey := key("user", selector, value)
	if get(cacheKey, output) {
		return output
	}
	database.Get().Model(output).Where(selector+" = ?", value).Scan(output)
	if output.ID == 0 {
		return nil
	}
	SetUser(output)
	return output
}

func SetUser(input *database.User) {
	set("user", input, 15*time.Minute)
}

func DelUser(input *database.User) {
	del("user", input)
}
