package model

import (
	"time"

	"gitlab.com/kotahkon/utilities/database"
)

func GetTarget(selector string, value interface{}) *database.Target {
	output := &database.Target{}
	if !hasSelector(selector, output) {
		return nil
	}
	cacheKey := key("target", selector, value)
	if get(cacheKey, output) {
		return output
	}
	database.Get().Model(output).Where(selector+" = ?", value).Scan(output)
	if output.ID == 0 {
		return nil
	}
	SetTarget(output)
	return output
}

func SetTarget(input *database.Target) {
	set("target", input, 15*time.Minute)
}

func DelTarget(input *database.Target) {
	del("target", input)
}
